# veille-techno


## Objectif

Réaliser une présentation de 10 à 15 minutes suivie d'une démo technique de 5 à 10 minutes sur un sujet technologique au choix dans une liste définie.


## Sujets

  - Mockery, MockMVC : Les tests unitaires et les tests d'intégration. Sujet choisi par **Jéhane** et **Thanmy**
  - Tests de charge et outils de profiling => **Kévin Rousseau** et **Quentin**
  - La qualité du code et SonarQube => **Nolwenn**, **Kévin Radlowski** et **Phonevilay**
  - Les tests de sécurité basique et le fuzzing => **Lélia**, **Fanny**
  - Les tests fonctionnels avec Sélénium et/ou Cucumber => Florian, Roland
  - i18n/l10n => **Mohamed-Amine**, **Daniela**


## Format de rendu
  - Un support de présentation type powerpoint
  - Un dépôt git public contenant la démonstration technique (les fichiers de configuration pour les outils de test)
